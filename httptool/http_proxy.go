package httptool

import (
	"net/http"
	"net/url"
)

const Proxy = "http://127.0.0.1:19999"

func HttpProxyGet(dstUrl string) (*http.Response, error) {
	transport := &http.Transport{Proxy: func(r *http.Request) (*url.URL, error) {
		return url.Parse(Proxy)
	}}
	client := &http.Client{Transport: transport}
	return client.Get(dstUrl)
}
