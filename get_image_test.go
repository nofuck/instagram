package instagram

import (
	"testing"
	"fmt"
)

func TestGetImage(t *testing.T) {
	url := "https://www.instagram.com/p/BZmLNftHGIR/"
	fmt.Printf("%+v\n", GetImage(url))
}

func TestGetImageUrl(t *testing.T) {
	url := "https://www.instagram.com/p/Ba4_00rHqfj/"
	sharedData := GetImage(url)
	imageUrl, _ := GetImageUrl(sharedData)
	fmt.Println(imageUrl)
}

func TestDownloadImage(t *testing.T) {
	url := "https://www.instagram.com/p/Ba4_00rHqfj/"
	sharedData := GetImage(url)
	imageUrls, _ := GetImageUrl(sharedData)
	for _, imageUrl := range imageUrls {
		SaveImage(imageUrl)
	}
}
